class MeetupsController < ApplicationController
  before_action :set_meetup, only: [:show, :update, :destroy]

  # GET /meetups
  # GET /meetups.json
  def index
    @meetups = Meetup.all
  end

  # GET /meetups/1
  # GET /meetups/1.json
  def show
  end

  # POST /meetups
  # POST /meetups.json
  def create
    @meetup = Meetup.new(meetup_params)

    if @meetup.save
      render :show, status: :created, location: @meetup
    else
      render json: @meetup.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /meetups/1
  # PATCH/PUT /meetups/1.json
  def update
    if @meetup.update(meetup_params)
      render :show, status: :ok, location: @meetup
    else
      render json: @meetup.errors, status: :unprocessable_entity
    end
  end

  # DELETE /meetups/1
  # DELETE /meetups/1.json
  def destroy
    @meetup.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meetup
      @meetup = Meetup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def meetup_params
      params.require(:meetup).permit(:name, :desc)
    end
end
