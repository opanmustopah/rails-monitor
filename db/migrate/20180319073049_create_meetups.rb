class CreateMeetups < ActiveRecord::Migration[5.1]
  def change
    create_table :meetups do |t|
      t.string :name, null: false, unique: true
      t.text :desc
      t.timestamps
    end
  end
end
