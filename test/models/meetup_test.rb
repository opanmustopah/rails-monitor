require 'test_helper'

class MeetupTest < ActiveSupport::TestCase
  setup do
    @meetup = Meetup.new
  end

  test 'have columns' do
    assert_respond_to @meetup, :name
    assert_respond_to @meetup, :desc
  end

  test '#validations' do
    assert_must validate_presence_of(:name), @meetup
  end
end
